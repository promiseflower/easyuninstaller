package com.eosr14.easyuninstaller;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StatFs;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final int MENU_DOWNLOAD = 0;
    private final int MENU_ALL = 1;
    private int MENU_MODE = MENU_DOWNLOAD;

    private ListView mAppListVIew;
    private TextView mBtnDelete, mTvStorage, mTvAppCount;
    private CheckBox mChkAllDelete;

    private PackageManager pm;
    private AppListAdapter mAdapter;
    private ProgressDialog mProgressDialog;

    private ArrayList<String> mPackageRemoveList;

    /******************************************************************************************************
     * Life Cycle
     *****************************************************************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindView();
    }

    @Override
    protected void onResume() {
        super.onResume();

        startTask();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /******************************************************************************************************
     * Method
     *****************************************************************************************************/
    private void bindView() {
        mTvStorage = (TextView) findViewById(R.id.tv_storage);
        mTvAppCount = (TextView) findViewById(R.id.tv_appCount);
        mAppListVIew = (ListView) findViewById(R.id.lv_appList);
        mBtnDelete = (TextView) findViewById(R.id.tv_delete);
        // mChkAllDelete = (CheckBox) findViewById(R.id.chk_allDelete);

        mAdapter = new AppListAdapter(MainActivity.this);
        mAppListVIew.setAdapter(mAdapter);

        mPackageRemoveList = new ArrayList<>();

        mBtnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // goDeleteApp(packageName);
                Toast.makeText(MainActivity.this, "삭제하기 버튼", Toast.LENGTH_SHORT).show();

                if (mPackageRemoveList.size() != 0) {
                    for (int i = 0; i < mPackageRemoveList.size(); i++) {
                        String listName = mPackageRemoveList.get(i).toString();
                        unInstallApp(listName);
                        // goDeleteApp(listName);
                    }
                }
            }
        });

        mAppListVIew.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mAdapter != null) {
                    mAdapter.setChecked(position);
                    mAdapter.notifyDataSetChanged();

                    boolean isCheckable = mAdapter.isCheckable(position);
                    Log.d("eosr14", "isCheckable : " + isCheckable);

                    String packageName = ((TextView) view.findViewById(R.id.tv_appCapacity)).getText().toString();
                    Toast.makeText(MainActivity.this, "Item packageName : " + packageName, Toast.LENGTH_SHORT).show();

                    if (isCheckable) {
                        if (mPackageRemoveList != null) {
                            // ArrayList Adding
                            mPackageRemoveList.add(packageName);
                        }
                    } else {
                        if (mPackageRemoveList != null) {
                            // ArrayList Remove
                            mPackageRemoveList.remove(packageName);
                        }
                    }

                    if (mPackageRemoveList.size() != 0) {
                        for (int i = 0; i < mPackageRemoveList.size(); i++) {
                            Log.d("eosr14", "mPackageRemoveList 데이터(0 아닐 때) : " + mPackageRemoveList.get(i).toString());
                        }
                        Log.d("eosr14", "mPackageRemoveList 데이터(0 아닐 때 size) : " + mPackageRemoveList.size());
                    } else {
                        Log.d("eosr14", "mPackageRemoveList 데이터(0 일때)");
                    }

                }
            }
        });
    }

    /******************************************************************************************************
     * Adapter Class
     *****************************************************************************************************/
    private class AppListAdapter extends BaseAdapter {

        private Context mContext;
        private ItemHolder mItemHolder;
        private List<ApplicationInfo> mAppList = null;
        private ArrayList<AppInfo> mListData = new ArrayList<AppInfo>();
        private boolean[] isCheckedConfrim;
        private boolean isFirstFlag;

        /******************************************************************************************************
         * Constructor
         *****************************************************************************************************/
        public AppListAdapter(Context context) {
            mContext = context;
            isFirstFlag = true;
        }

        @Override
        public int getCount() {
            return mListData.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                mItemHolder = new ItemHolder();
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_applist, null);

                mItemHolder.mAppIcon = (ImageView) convertView.findViewById(R.id.iv_appIcon);
                mItemHolder.mAppName = (TextView) convertView.findViewById(R.id.tv_appName);
                mItemHolder.mAppCapacity = (TextView) convertView.findViewById(R.id.tv_appCapacity);
                mItemHolder.mChkSelectDelete = (CheckBox) convertView.findViewById(R.id.chk_app);
                convertView.setTag(mItemHolder);
            } else {
                mItemHolder = (ItemHolder) convertView.getTag();
            }

            //Log.d("eosr14", "mListData null? : " + mListData);
            AppInfo data = mListData.get(position);

            if (isFirstFlag) {
                isCheckedConfrim = new boolean[mListData.size()];
                isFirstFlag = false;
            }

            if (data.mIcon != null) {
                mItemHolder.mAppIcon.setImageDrawable(data.mIcon);
            }
            mItemHolder.mAppName.setText(data.mAppName);
            mItemHolder.mAppCapacity.setText(data.mAppPackage);

            // CheckBox
            mItemHolder.mChkSelectDelete.setFocusable(false);
            mItemHolder.mChkSelectDelete.setClickable(false);
            if (isCheckedConfrim != null) {
                try {
                    mItemHolder.mChkSelectDelete.setChecked(isCheckedConfrim[position]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return convertView;
        }

        public boolean isCheckable(int position) {
            Log.d("eosr14", "isCheckable position : " + position);
            boolean isCheck = isCheckedConfrim[position];
            return isCheck;
        }

        public void setChecked(int position) {
            isCheckedConfrim[position] = !isCheckedConfrim[position];
        }

        public void rebuild() {
            if (mAppList == null) {
                pm = MainActivity.this.getPackageManager();

                mAppList = pm
                        .getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES
                                | PackageManager.GET_DISABLED_COMPONENTS);
            }

            AppInfo.AppFilter filter;
            switch (MENU_MODE) {
                case MENU_DOWNLOAD:
                    filter = AppInfo.THIRD_PARTY_FILTER;
                    break;
                default:
                    filter = null;
                    break;
            }

            if (filter != null) {
                filter.init();
            }

            mListData.clear();

            AppInfo addInfo = null;
            ApplicationInfo info = null;
            for (ApplicationInfo app : mAppList) {
                info = app;

                if (filter == null || filter.filterApp(info)) {
                    addInfo = new AppInfo();
                    // App Icon
                    addInfo.mIcon = app.loadIcon(pm);
                    // App Name
                    addInfo.mAppName = app.loadLabel(pm).toString();
                    // App Package Name
                    addInfo.mAppPackage = app.packageName;
                    mListData.add(addInfo);
                }
            }

            Collections.sort(mListData, AppInfo.ALPHA_COMPARATOR);
        }

        /******************************************************************************************************
         * Inner Class
         *****************************************************************************************************/
        private class ItemHolder {
            private ImageView mAppIcon;
            private TextView mAppName;
            private TextView mAppCapacity;
            private CheckBox mChkSelectDelete;
        }

    }

    /******************************************************************************************************
     * AsyncTask Class
     *****************************************************************************************************/
    private class AppTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            mProgressDialog = new ProgressDialog(MainActivity.this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setMessage("로딩 중 입니다...");
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (mAdapter != null) {
                mAdapter.rebuild();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (mAdapter != null) {
                mTvAppCount.setText("앱 개수 : " + mAdapter.getCount() + "개");

                String totalSize = sizeFormat(getTotalExternalMemorySize());
                String availableSize = sizeFormat(getAvailableInternalMemorySize());

                mTvStorage.setText("용량 : " + availableSize + "/" + totalSize);
            }

            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }

    }

    private void startTask() {
        new AppTask().execute();
    }

    /******************************************************************************************************
     * Method
     *****************************************************************************************************/
    // 총 외장 메모리 용량 구하기
    public long getTotalExternalMemorySize() {
        if (IsExternalMemoryAvailable()) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long totalBlocks = stat.getBlockCount();
            return totalBlocks * blockSize;
        } else {
            return -1;
        }
    }

    // 남은 외장 메모리 용량 구하기
    public long getAvailableExternalMemorySize() {
        long nSize = -1;

        if (IsExternalMemoryAvailable()) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long availableBlocks = stat.getAvailableBlocks();

            nSize = availableBlocks * blockSize;
        }
        return nSize;
    }

    // 총 내장 메모리 용량 구하기
    public long getTotalInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return totalBlocks * blockSize;
    }

    // 남은 내장 메모리 용량 구하기
    public long getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return availableBlocks * blockSize;
    }

    private static boolean IsExternalMemoryAvailable() {
        return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }

    public String sizeFormat(double bytesSize) {
        int unit = 1024;
        if (bytesSize < unit) return bytesSize + " B";
        int exp = (int) (Math.log(bytesSize) / Math.log(unit));
        String pre = "KMGTPE".charAt(exp - 1) + "";
        return String.format("%.2f %sB", bytesSize / Math.pow(unit, exp), pre);
    }

    private void goDeleteApp(String packageName) {
        Uri packageURI = Uri.parse("package:" + packageName);
        Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(uninstallIntent);
    }

    private void unInstallApp(String packageName) {
        Uri uri = Uri.fromParts("package", packageName, null);
        Intent intent = new Intent(Intent.ACTION_DELETE, uri);
        startActivity(intent);
    }

} // end of class MainActivity
